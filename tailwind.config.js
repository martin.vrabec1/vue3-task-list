/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,vue}"],
  theme: {
    extend: {
      colors: {
        'pink': '#3E1671',
        'light-pink': '#9E78CF',
        'silver': '#777777',
        'black': '#0D0714',
        'black2': '#15101C',
        'green': '#78CFB0'
      },
    },
  },
  plugins: [],
}

