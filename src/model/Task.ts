type Task = {
    name: string,
    active: boolean,
    id: string
}


export default Task