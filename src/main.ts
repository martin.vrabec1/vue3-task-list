import { createApp } from 'vue'
import App from './App.vue'
import { toast } from 'vue3-toastify';
import './assets/style.css'
import 'vue3-toastify/dist/index.css';

createApp(App).mount('#app')
